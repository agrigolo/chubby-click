package agrigolo.chubbyclick;

public class Metronome {

    private double bpm;
    private int beat;
    private int silence;

    private final int tick = 1000; // samples of tick

    private boolean play = false;

    private AudioGenerator audioGenerator = new AudioGenerator(8000);

    public Metronome(double bpm, int beat) {
        audioGenerator.createPlayer();
        this.bpm = bpm;
        this.beat = beat;
    }

    public void calcSilence() {
        silence = (int) (((60 / bpm) * 8000) - tick);
    }

    public void play() {

        this.play = true;
        double[] highTick =
                audioGenerator.getSineWave(this.tick, 8000, 600);
        double[] lowTick =
                audioGenerator.getSineWave(this.tick, 8000, 450);
        if (beat == 0) {
            highTick = lowTick;
        }
        double silence = 0;
        double[] sound = new double[8000];
        int t = 0, s = 0, b = 0;
        do {
            //Calculate silence duration in every cycle to change the bpm without restarting the task
            calcSilence();
            for (int i = 0; i < sound.length && play; i++) {
                if (t < this.tick) {
                    if (b == 0)
                        sound[i] = highTick[t];
                    else
                        sound[i] = lowTick[t];
                    t++;
                } else {
                    sound[i] = silence;
                    s++;
                    if (s >= this.silence) {
                        t = 0;
                        s = 0;
                        b++;
                        if (b > (this.beat - 1))
                            b = 0;
                    }
                }
            }
            audioGenerator.writeSound(sound);
        } while (this.play);
    }

    public void stop() {
        this.play = false;
        audioGenerator.destroyAudioTrack();
    }

    /* Getters and Setters*/

    public boolean isPlaying() {
        return this.play;
    }

    public void bpm(double new_bpm) {
        this.bpm = new_bpm;
    }

}