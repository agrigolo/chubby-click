package agrigolo.chubbyclick;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    private SharedPreferences sharedPreferences;
    private static String PREF_NAME = "prefs";

    public Preferences() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getBpm(Context context, String bpm) {
        return getPrefs(context).getString("bpm", "100");
    }

    public static void setBpm(Context context, String input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString("bpm", input);
        editor.commit();
    }

    public static String getBeat(Context context, String beat) {
        return getPrefs(context).getString("beat", "4");
    }

    public static void setBeat(Context context, String input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString("beat", input);
        editor.commit();
    }
    
}