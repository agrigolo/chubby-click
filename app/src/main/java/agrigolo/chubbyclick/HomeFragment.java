package agrigolo.chubbyclick;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;

import agrigolo.chubbyclick.metronome.Metronome;
import agrigolo.chubbyclick.utilities.Preferences;
import agrigolo.chubbyclick.utilities.TapTempo;

public class HomeFragment extends androidx.fragment.app.Fragment {

    private boolean soundDialogOpened = false;
    private Preferences prefs;
    private double current_bpm;
    private int current_beat;
    private Context myContext;
    private TapTempo taptempo = new TapTempo();
    private Metronome metronome;
    private String beatSubdivisions;
    private OnFragmentInteractionListener mListener;
    private View v;
    private BroadcastReceiver br;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_home, null, false);

        Toolbar toolbar = v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        prefs = new Preferences();

        current_beat = Integer.parseInt(Preferences.getBeat());
        current_bpm = Double.parseDouble(Preferences.getBpm());
        beatSubdivisions = prefs.getBeatSubdivisions();

        //Sets default beatSubdivision based on current beat for first usage and for retrocompatibility
        if (beatSubdivisions.equals("")) {
            for (int i = 0; i < current_beat; i++) {
                if (i == 0) beatSubdivisions = "a";
                else beatSubdivisions += "b";
            }
            setBeatSubdivisions(current_beat);
            prefs.setBeatSubdivisions(beatSubdivisions);
        }

        metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
        createSubdivisionsButtons(current_beat);
        setBeatButtonsClickListeners(current_beat);

        final SeekBar seekbar = v.findViewById(R.id.tempo_seek);
        final SeekBar volumebar = v.findViewById(R.id.volume_seek);
        final TextView bpmText = v.findViewById(R.id.bpm_text);
        final TextView beatText = v.findViewById(R.id.input_beats);
        beatText.setText(String.valueOf(current_beat));

        seekbar.setProgress((int) current_bpm);
        bpmText.setText(String.valueOf((int) current_bpm));

        final Button startButton = v.findViewById(R.id.start_stop);
        final Button tapTempoButton = v.findViewById(R.id.tap_tempo);

        String title = "Tap Tempo";
        String subTitle = "Hold to reset";
        int titleLength = title.length();
        int subtitleLength = subTitle.length();
        Spannable span = new SpannableString(title + "\n" + subTitle);
        span.setSpan(new RelativeSizeSpan(0.6f), titleLength, (titleLength + subtitleLength + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tapTempoButton.setText(span);

        final Button minusOneButton = v.findViewById(R.id.minus_1);
        final Button minusTenButton = v.findViewById(R.id.minus_10);
        final Button plusOneButton = v.findViewById(R.id.plus_1);
        final Button plusTenButton = v.findViewById(R.id.plus_10);
        final Button beatPlusButton = v.findViewById(R.id.beat_plus);
        final Button beatMinusButton = v.findViewById(R.id.beat_minus);

        double current_volume = Double.parseDouble(prefs.getVolume());
        volumebar.setProgress((int) current_volume);
        setVolumeImage(current_volume);

        startButton.setText(getResources().getString(R.string.text_start));

        tapTempoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!taptempo.isTapping()) {
                    Snackbar.make(v, getResources().getString(R.string.taptempo_reset), 4000).show();
                }
                taptempo.tap();
                current_bpm = taptempo.getAvgBpm();
                change_bpm(current_bpm);
            }
        });
        tapTempoButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                taptempo.reset();
                current_bpm = taptempo.getAvgBpm();
                change_bpm(current_bpm);
                return true;
            }
        });
        minusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm > 1) {
                    current_bpm -= 1;
                    change_bpm(current_bpm);
                }
            }
        });
        minusTenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm > 10) {
                    current_bpm -= 10;
                    change_bpm(current_bpm);
                }
            }
        });
        plusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm < 400) {
                    current_bpm += 1;
                    change_bpm(current_bpm);
                }
            }
        });
        plusTenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm <= 390) {
                    current_bpm += 10;
                    change_bpm(current_bpm);
                }
            }
        });
        beatMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_beat > 1) {
                    current_beat -= 1;
                    beatText.setText(String.valueOf(current_beat));
                    prefs.setBeat(String.valueOf(current_beat));
                    createSubdivisionsButtons(current_beat);
                    setBeatButtonsClickListeners(current_beat);
                    setBeatSubdivisions(current_beat);
                    if (metronome.isPlaying()) {
                        restartClick();
                    } else metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
                }
            }
        });

        beatPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_beat < 16) {
                    current_beat += 1;
                    beatText.setText(String.valueOf(current_beat));
                    prefs.setBeat(String.valueOf(current_beat));
                    setBeatSubdivisions(current_beat);
                    createSubdivisionsButtons(current_beat);
                    setBeatButtonsClickListeners(current_beat);
                    if (metronome.isPlaying()) {
                        restartClick();
                    } else metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
                }
            }
        });

        beatText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                current_beat = Integer.parseInt(beatText.getText().toString());
                metronome.setBeat(current_beat);
                prefs.setBeat(String.valueOf(current_beat));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                playPause();
            }
        });

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                playPause();
            }
        };

        IntentFilter filter = new IntentFilter("agrigolo.chubbyclick.PLAY_PAUSE");
        getActivity().registerReceiver(br, filter);


        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                bpmText.setText("" + i);
                current_bpm = i;
                metronome.setBpm(current_bpm);
                Preferences.setBpm(String.valueOf(current_bpm));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });

        volumebar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                double current_volume = i;
                metronome.setVolume(current_volume);
                prefs.setVolume(Double.toString(current_volume));
                setVolumeImage(current_volume);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.main_menu, menu);

        if (prefs.getScreenAlwaysOn().equals("true")) {
            menu.findItem(R.id.action_menu_screen_on).setChecked(true);
        } else if (prefs.getScreenAlwaysOn().equals("false")) {
            menu.findItem(R.id.action_menu_screen_on).setChecked(false);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_settings:
                soundSettingsDialog();
                return true;
            case R.id.action_menu_about:
                aboutDialog();
                return true;
            case R.id.action_menu_screen_on:
                if (item.isChecked()) {
                    item.setChecked(false);
                    prefs.setScreenAlwaysOn("false");
                } else {
                    item.setChecked(true);
                    prefs.setScreenAlwaysOn("true");
                }
                ((MainActivity) getActivity()).screenAlwaysOn();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setBeatSubdivisions(int beats) {
        for (int i = 0; i < beats; i++) {
            if (i == 0) beatSubdivisions = "a";
            else beatSubdivisions += "b";
        }
        prefs.setBeatSubdivisions(beatSubdivisions);
    }

    private void createSubdivisionsButtons(int beats) {
        int buttonsNumber = beats; // Number of buttons
        LinearLayout col0 = this.v.findViewById(R.id.col0);
        col0.removeAllViewsInLayout();

        for (int i = 0; i < buttonsNumber; i++) {
            try {
                Button newButton = new Button(myContext);
                newButton.setId(R.id.class.getField("b" + i).getInt(null));
                newButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));

                switch (beatSubdivisions.charAt(i)) {
                    //Accent
                    case 'a':
                        newButton.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.accent));
                        newButton.setAlpha(1f);
                        break;
                    //Beat
                    case 'b':
                        newButton.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.beat));
                        newButton.setAlpha(1f);
                        break;
                    //Mute
                    case 'm':
                        newButton.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.beat));
                        newButton.setAlpha(0.2f);
                        break;
                }

                col0.addView(newButton);
            } catch (Exception e) {

            }
        }
    }

    private void playPause() {
        Button startButton = this.v.findViewById(R.id.start_stop);
        if (soundDialogOpened) {
            metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
            soundDialogOpened = false;
        }
        if (!metronome.isPlaying()) {
            metronome.execute();
            startButton.setText(getResources().getString(R.string.text_stop));
        } else {
            metronome.stop();
            metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
            startButton.setText(getResources().getString(R.string.text_start));
        }
    }

    private void setBeatButtonsClickListeners(int beats) {
        int buttonsNumber = beats; // Number of buttons
        for (int i = 0; i < buttonsNumber; i++) {
            final int id = getResources().getIdentifier("b" + i, "id", myContext.getPackageName());
            final int finalI = i;
            final StringBuilder[] tempString = new StringBuilder[1];
            v.findViewById(id).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (beatSubdivisions.charAt(finalI)) {
                        //Cycle to beat
                        case 'a':
                            view.setAlpha(1f);
                            view.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.beat));
                            tempString[0] = new StringBuilder(beatSubdivisions);
                            tempString[0].setCharAt(finalI, 'b');
                            beatSubdivisions = tempString[0].toString();
                            metronome.setBeatSubdivisions(beatSubdivisions);
                            prefs.setBeatSubdivisions(beatSubdivisions);
                            break;
                        //Cycle to mute
                        case 'b':
                            view.setAlpha(0.2f);
                            view.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.beat));
                            tempString[0] = new StringBuilder(beatSubdivisions);
                            tempString[0].setCharAt(finalI, 'm');
                            beatSubdivisions = tempString[0].toString();
                            metronome.setBeatSubdivisions(beatSubdivisions);
                            prefs.setBeatSubdivisions(beatSubdivisions);
                            break;
                        //Cycle to accent
                        case 'm':
                            view.setAlpha(1f);
                            view.setBackgroundTintList(myContext.getResources().getColorStateList(R.color.accent));
                            tempString[0] = new StringBuilder(beatSubdivisions);
                            tempString[0].setCharAt(finalI, 'a');
                            beatSubdivisions = tempString[0].toString();
                            metronome.setBeatSubdivisions(beatSubdivisions);
                            prefs.setBeatSubdivisions(beatSubdivisions);
                            break;
                    }
                }
            });
        }
    }


    private void restartClick() {
        metronome.stop();
        metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
        metronome.execute();
    }

    private void change_bpm(double bpm) {
        SeekBar seekbar = this.v.findViewById(R.id.tempo_seek);
        TextView bpmText = this.v.findViewById(R.id.bpm_text);
        bpmText.setText("" + (int) bpm);
        metronome.setBpm(bpm);
        seekbar.setProgress((int) bpm);
        Preferences.setBpm(String.valueOf(bpm));
    }

    private void setVolumeImage(double current_volume) {
        ImageView volumeImage = this.v.findViewById(R.id.volumeImage);
        if (current_volume == 0) {
            volumeImage.setImageResource(R.drawable.volume_off);
        } else if (current_volume > 0 && current_volume <= 50) {
            volumeImage.setImageResource(R.drawable.volume_low);
        } else if (current_volume > 50) {
            volumeImage.setImageResource(R.drawable.volume_high);
        }
    }

    private void aboutDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(myContext);
        dialog.setMessage(getResources().getString(R.string.about_message));
        dialog.setTitle("Chubby Click " + BuildConfig.VERSION_NAME);
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                    }
                });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private void soundSettingsDialog() {
        stopClick();
        soundDialogOpened = true;
        soundSettingsDialog soundDialog = new soundSettingsDialog();
        soundDialog.showDialog(current_bpm, current_beat, myContext);

        Button startButton = this.v.findViewById(R.id.start_stop);
        startButton.setText(getResources().getString(R.string.text_start));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            myContext = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        stopClick();
        mListener = null;
        myContext.unregisterReceiver(br);
        super.onDetach();
    }

    private void stopClick() {
        if (metronome.isPlaying()) {
            metronome.stop();
            metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}