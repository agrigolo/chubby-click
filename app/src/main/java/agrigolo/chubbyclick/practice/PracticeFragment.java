package agrigolo.chubbyclick.practice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import agrigolo.chubbyclick.R;
import agrigolo.chubbyclick.metronome.Metronome;
import agrigolo.chubbyclick.utilities.Preferences;

public class PracticeFragment extends Fragment {

    private Metronome metronome = new Metronome(4, 4, "abbb");
    private Double startBPM;
    private Double endBPM;
    private double current_bpm;
    private int current_beat = Integer.parseInt(Preferences.getBeat());
    private String beatSubdivisions = Preferences.getBeatSubdivisions();
    private boolean validationErrors = false;
    private View v;
    private BroadcastReceiver br;
    private String practiceMode = "increase-decrease";
    private int playBars;
    private int muteBars;
    private int tempMeasure = 0;
    private String muteSubDivisions = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_practice, parent, false);

        Toolbar toolbar = v.findViewById(R.id.toolbar);
        toolbar.setSubtitle(Html.fromHtml("<small>" + getResources().getString(R.string.text_practice_subtitle) + "</small>"));

        final Button startButton = v.findViewById(R.id.start_stop);
        final Button incDecModeButton = v.findViewById(R.id.mode_increase_decrease);
        final Button gapsModeButton = v.findViewById(R.id.mode_gaps);
        final TextView bpmStart = v.findViewById(R.id.bpm_start_value);
        final TextView bpmChangeValue = v.findViewById(R.id.bpm_change_value);
        final TextView measures = v.findViewById(R.id.measures_value);
        final TextView bpmEnd = v.findViewById(R.id.until_value);
        final TextView gapsPlayValue = v.findViewById(R.id.gaps_play_value);
        final TextView gapsMuteValue = v.findViewById(R.id.gaps_mute_value);

        syncPreferences("load");
        setPracticeMode("increase-decrease");

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                playPauseIncDec();
            }
        };

        IntentFilter filter = new IntentFilter("agrigolo.chubbyclick.PLAY_PAUSE");
        getActivity().registerReceiver(br, filter);

        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (practiceMode) {
                    case "increase-decrease":
                        playPauseIncDec();
                        break;
                    case "gaps":
                        playPauseGaps();
                        break;
                }
            }
        });

        //Increase-Decrease view elements
        incDecModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPracticeMode("increase-decrease");
            }
        });

        gapsModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPracticeMode("gaps");
            }
        });

        List<TextView> practiceTextViews = new ArrayList<TextView>() {
            {
                add(bpmStart);
                add(bpmEnd);
                add(measures);
                add(bpmChangeValue);
                add(gapsPlayValue);
                add(gapsMuteValue);
            }
        };

        for (final TextView textView : practiceTextViews)
            textView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    validateNumber(textView.getId());
                }
            });

        return v;
    }

    //Hide or show UI elements depending on the selected Practice Mode
    private void setPracticeMode(String mode) {

        Button incDecModeButton = v.findViewById(R.id.mode_increase_decrease);
        Button gapsModeButton = v.findViewById(R.id.mode_gaps);
        TextView bpmStartLabel = v.findViewById(R.id.bpm_start);
        Button startButton = v.findViewById(R.id.start_stop);

        List<View> increaseDecreaseViews = new ArrayList<>();
        List<View> gapsViews = new ArrayList<>();

        increaseDecreaseViews.add(v.findViewById(R.id.measures_every));
        increaseDecreaseViews.add(v.findViewById(R.id.until_bpm));
        increaseDecreaseViews.add(v.findViewById(R.id.bpm_change));
        increaseDecreaseViews.add(v.findViewById(R.id.measures_every));
        increaseDecreaseViews.add(v.findViewById(R.id.measures_measures));
        increaseDecreaseViews.add(v.findViewById(R.id.until));
        increaseDecreaseViews.add(v.findViewById(R.id.bpm_change_value));
        increaseDecreaseViews.add(v.findViewById(R.id.measures_value));
        increaseDecreaseViews.add(v.findViewById(R.id.until_value));
        increaseDecreaseViews.add(v.findViewById(R.id.increase_decrease_group));

        gapsViews.add(v.findViewById(R.id.gaps_play_text));
        gapsViews.add(v.findViewById(R.id.gaps_play_value));
        gapsViews.add(v.findViewById(R.id.gaps_play_bars_text));
        gapsViews.add(v.findViewById(R.id.gaps_mute_text));
        gapsViews.add(v.findViewById(R.id.gaps_mute_value));
        gapsViews.add(v.findViewById(R.id.gaps_mute_bars_Text));

        if(metronome.isPlaying()) {
            metronome.stop();
            startButton.setText(getResources().getString(R.string.text_start));
        }

        switch (mode) {
            //Increase-Decrease Tempo Mode
            case "increase-decrease":
                practiceMode = "increase-decrease";
                for (final View practiceView : increaseDecreaseViews) {
                    practiceView.setVisibility(View.VISIBLE);
                }
                for (final View practiceView : gapsViews) {
                    practiceView.setVisibility(View.GONE);
                }

                incDecModeButton.setBackgroundTintList(ContextCompat.getColorStateList(v.getContext(), R.color.accent));
                gapsModeButton.setBackgroundTintList(ContextCompat.getColorStateList(v.getContext(), R.color.beat));
                bpmStartLabel.setText(R.string.start_bpm_label);

                break;
            //GAPS mode
            case "gaps":
                practiceMode = "gaps";
                for (final View practiceView : increaseDecreaseViews) {
                    practiceView.setVisibility(View.GONE);
                }
                for (final View practiceView : gapsViews) {
                    practiceView.setVisibility(View.VISIBLE);
                }

                incDecModeButton.setBackgroundTintList(ContextCompat.getColorStateList(v.getContext(), R.color.beat));
                gapsModeButton.setBackgroundTintList(ContextCompat.getColorStateList(v.getContext(), R.color.accent));
                bpmStartLabel.setText(R.string.practice_BPM);
                break;
        }
    }

    private void playPauseIncDec() {
        tempMeasure = 0;

        final Button startButton = v.findViewById(R.id.start_stop);
        final TextView currentBPMValue = v.findViewById(R.id.bpm_text);
        final RadioGroup increase_decrease = v.findViewById(R.id.increase_decrease_group);
        final TextView bpmStart = v.findViewById(R.id.bpm_start_value);
        final TextView bpmChangeValue = v.findViewById(R.id.bpm_change_value);
        final TextView measures = v.findViewById(R.id.measures_value);
        final TextView bpmEnd = v.findViewById(R.id.until_value);

        if (!metronome.isPlaying()) {
            if (validationErrors == true) {
                Snackbar.make(this.v, getResources().getString(R.string.practice_empty_fields), 2000).show();
                return;
            }

            startBPM = Double.parseDouble(bpmStart.getText().toString());
            endBPM = Double.parseDouble(bpmEnd.getText().toString());

            if (increase_decrease.getCheckedRadioButtonId() == R.id.increase_button) {
                if (endBPM < startBPM) {
                    Snackbar.make(v, getResources().getString(R.string.practice_error_1), 2000).show();
                    return;
                }
            }
            if (increase_decrease.getCheckedRadioButtonId() == R.id.decrease_button) {
                if (endBPM > startBPM) {
                    Snackbar.make(v, getResources().getString(R.string.practice_error_2), 2000).show();
                    return;
                }
            }
            final Double bpmChange = Double.parseDouble(bpmChangeValue.getText().toString());
            final int changeMeasures = Integer.parseInt(measures.getText().toString());

            current_bpm = startBPM;
            currentBPMValue.setText(String.valueOf((int) current_bpm));
            metronome = new Metronome(startBPM, current_beat, beatSubdivisions);
            metronome.execute();
            startButton.setText(getResources().getString(R.string.text_stop));

            metronome.setMetronomeListener(new Metronome.MetronomeListener() {
                @Override
                public void onMeasureChange(int current_measure) {
                    tempMeasure++;
                    int selected = increase_decrease.getCheckedRadioButtonId();
                    switch (selected) {
                        case R.id.increase_button:
                            if ((current_bpm + bpmChange <= endBPM) && (tempMeasure == changeMeasures)) {
                                current_bpm += bpmChange;
                                metronome.setBpm(current_bpm);
                                tempMeasure = 0;
                                currentBPMValue.setText(String.valueOf((int) current_bpm));
                            }
                            break;
                        case R.id.decrease_button:
                            if (current_bpm - bpmChange >= endBPM && (tempMeasure == changeMeasures)) {
                                current_bpm -= bpmChange;
                                metronome.setBpm(current_bpm);
                                tempMeasure = 0;
                                currentBPMValue.setText(String.valueOf((int) current_bpm));
                            }
                            break;
                    }
                }

            });
        } else {
            metronome.stop();
            startButton.setText(getResources().getString(R.string.text_start));
        }
    }

    private void playPauseGaps() {
        tempMeasure = 0;
        //Beat Subdivision string with avery beat set as mute
        muteSubDivisions = beatSubdivisions.toLowerCase().replaceAll(".", "m");

        final Button startButton = v.findViewById(R.id.start_stop);
        final TextView currentBPMValue = v.findViewById(R.id.bpm_text);
        final TextView bpmStart = v.findViewById(R.id.bpm_start_value);
        final TextView gapsPlayValue = v.findViewById(R.id.gaps_play_value);
        final TextView gapsMuteValue = v.findViewById(R.id.gaps_mute_value);
        playBars = Integer.parseInt(gapsPlayValue.getText().toString());
        muteBars = Integer.parseInt(gapsMuteValue.getText().toString());

        if (!metronome.isPlaying()) {
            if (validationErrors == true) {
                Snackbar.make(this.v, getResources().getString(R.string.practice_empty_fields), 2000).show();
                return;
            }

            startBPM = Double.parseDouble(bpmStart.getText().toString());
            current_bpm = startBPM;
            currentBPMValue.setText(String.valueOf((int) current_bpm));
            metronome = new Metronome(startBPM, current_beat, beatSubdivisions);
            metronome.execute();
            startButton.setText(getResources().getString(R.string.text_stop));

            metronome.setMetronomeListener(new Metronome.MetronomeListener() {
                @Override
                public void onMeasureChange(int current_measure) {
                    if (tempMeasure < (playBars + muteBars - 1)) {
                        tempMeasure++;
                    } else {
                        tempMeasure = 0;
                    }
                    if (tempMeasure == playBars) {
                        //MUTE BARS
                        metronome.setBeatSubdivisions(muteSubDivisions);
                    } else if (tempMeasure == 0) {
                        //STANDARD BARS
                        metronome.setBeatSubdivisions(beatSubdivisions);
                    }
                }
            });
        } else {
            metronome.stop();
            startButton.setText(getResources().getString(R.string.text_start));
        }
    }

    private void validateNumber(int id) {
        TextView textView = v.findViewById(id);
        int currentValue;
        try {
            currentValue = Integer.parseInt(textView.getText().toString());
            if (currentValue == 0) {
                textView.setError(getResources().getString(R.string.practice_minimum_value));
                validationErrors = true;
            } else if (currentValue > 400) {
                textView.setError(getResources().getString(R.string.practice_maximum_value));
                validationErrors = true;
            } else validationErrors = false;
        } catch (NumberFormatException ex) {
            textView.setError(getResources().getString(R.string.practice_minimum_value));
            validationErrors = true;
        }
    }

    private void syncPreferences(String mode) {
        TextView bpmStart = v.findViewById(R.id.bpm_start_value);
        TextView bpmEnd = v.findViewById(R.id.until_value);
        TextView bpmChangeValue = v.findViewById(R.id.bpm_change_value);
        TextView measures = v.findViewById(R.id.measures_value);
        RadioGroup increase_decrease = v.findViewById(R.id.increase_decrease_group);
        TextView gapsPlayValue = v.findViewById(R.id.gaps_play_value);
        TextView gapsMuteValue = v.findViewById(R.id.gaps_mute_value);

        switch (mode) {
            case "save":
                Preferences.setPracticeStartBPM(bpmStart.getText().toString());
                Preferences.setPracticeEndBPM(bpmEnd.getText().toString());
                Preferences.setPracticeChangeBPM(bpmChangeValue.getText().toString());
                Preferences.setPracticeMeasures(measures.getText().toString());
                RadioButton sel = v.findViewById(increase_decrease.getCheckedRadioButtonId());
                String selText = (String) sel.getText();
                Preferences.setPracticeIncreaseDecrease(selText);
                Preferences.setPracticePlayBars(gapsPlayValue.getText().toString());
                Preferences.setPracticeMuteBars(gapsMuteValue.getText().toString());
                break;
            case "load":
                bpmStart.setText(Preferences.getPracticeStartBPM());
                bpmEnd.setText(Preferences.getPracticeEndBPM());
                bpmChangeValue.setText(Preferences.getPracticeChangeBPM());
                measures.setText(Preferences.getPracticeMeasures());
                gapsPlayValue.setText(Preferences.getPracticePlayBars());
                gapsMuteValue.setText(Preferences.getPracticeMuteBars());

                switch (Preferences.getPracticeIncreaseDecrease()) {
                    case "increase":
                        increase_decrease.check(R.id.increase_button);
                        break;
                    case "decrease":
                        increase_decrease.check(R.id.decrease_button);
                        break;
                }
                break;
        }
    }

    private void stopClick() {
        if (metronome.isPlaying()) {
            metronome.stop();
            tempMeasure = 0;
            metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
        }
    }

    @Override
    public void onDetach() {
        stopClick();
        syncPreferences("save");
        getContext().unregisterReceiver(br);
        super.onDetach();
    }

}