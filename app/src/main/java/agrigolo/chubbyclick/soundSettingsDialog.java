package agrigolo.chubbyclick;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import agrigolo.chubbyclick.metronome.Metronome;
import agrigolo.chubbyclick.utilities.Preferences;

class soundSettingsDialog {

    private int minDuration = 20;
    private int maxDuration = 1200;
    private int minPitch = 20;
    private int maxPitch = 3999;

    private Preferences prefs = new Preferences();

    private int currentNoteDuration = Integer.parseInt(prefs.getNoteDuration());
    private int currentBeatPitch = Integer.parseInt(prefs.getBeatPitch());
    private int currentAccentPitch = Integer.parseInt(prefs.getAccentPitch());

    private TextView beatText;
    private TextView accentText;
    private TextView noteDurationText;

    void showDialog(double bpm, int beat, Context context) {

        final Metronome metronome = new Metronome(bpm, beat, prefs.getBeatSubdivisions());

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.sound_settings_dialog, null, true);
        dialog.setView(viewInflated);
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        prefs.setNoteDuration(String.valueOf(currentNoteDuration));
                        prefs.setAccentPitch(String.valueOf(currentAccentPitch));
                        prefs.getBeatPitch(String.valueOf(currentBeatPitch));
                    }
                });
        dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final ImageButton beatPreviewButton = viewInflated.findViewById(R.id.preview_beat);
        final ImageButton accentPreviewButton = viewInflated.findViewById(R.id.preview_accent);
        final Button resetDefaults = viewInflated.findViewById(R.id.sound_setting_reset_button);
        final Button resetNatural = viewInflated.findViewById(R.id.sound_setting_natural_button);

        beatText = viewInflated.findViewById(R.id.beat_pitch_text);
        accentText = viewInflated.findViewById(R.id.accent_pitch_text);
        noteDurationText = viewInflated.findViewById(R.id.note_duration_text);

        final Button durationMinusTen = viewInflated.findViewById(R.id.duration_minus_ten);
        final Button durationMinusOne = viewInflated.findViewById(R.id.duration_minus_one);
        final Button durationPlusTen = viewInflated.findViewById(R.id.duration_plus_ten);
        final Button durationPlusOne = viewInflated.findViewById(R.id.duration_plus_one);
        final Button accentMinusTen = viewInflated.findViewById(R.id.accent_pitch_minus_ten);
        final Button accentMinusOne = viewInflated.findViewById(R.id.accent_pitch_minus_one);
        final Button accentPlusTen = viewInflated.findViewById(R.id.accent_pitch_plus_ten);
        final Button accentPlusOne = viewInflated.findViewById(R.id.accent_pitch_plus_one);
        final Button beatMinusTen = viewInflated.findViewById(R.id.beat_pitch_minus_ten);
        final Button beatMinusOne = viewInflated.findViewById(R.id.beat_pitch_minus_one);
        final Button beatPlusTen = viewInflated.findViewById(R.id.beat_pitch_plus_ten);
        final Button beatPlusOne = viewInflated.findViewById(R.id.beat_pitch_plus_one);

        final SeekBar durationBar = viewInflated.findViewById(R.id.durationSeekBar);
        final SeekBar accentBar = viewInflated.findViewById(R.id.accentSeekBar);
        final SeekBar beatBar = viewInflated.findViewById(R.id.beatSeekBar);

        durationBar.setProgress(currentNoteDuration);
        accentBar.setProgress(currentAccentPitch);
        beatBar.setProgress(currentBeatPitch);

        noteDurationText.setText(String.valueOf(currentNoteDuration));
        beatText.setText(String.valueOf(currentBeatPitch));
        accentText.setText(String.valueOf(currentAccentPitch));

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        durationMinusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNoteDuration >= (minDuration + 10)) {
                    currentNoteDuration -= 10;
                    noteDurationText.setText(String.valueOf(currentNoteDuration));
                    durationBar.setProgress(currentNoteDuration);
                }
            }
        });
        durationMinusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNoteDuration >= (minDuration + 1)) {
                    currentNoteDuration -= 1;
                    noteDurationText.setText(String.valueOf(currentNoteDuration));
                    durationBar.setProgress(currentNoteDuration);

                }
            }
        });
        durationPlusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNoteDuration <= (maxDuration - 10)) {
                    currentNoteDuration += 10;
                    noteDurationText.setText(String.valueOf(currentNoteDuration));
                    durationBar.setProgress(currentNoteDuration);

                }
            }
        });
        durationPlusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNoteDuration <= (maxDuration - 1)) {
                    currentNoteDuration += 1;
                    noteDurationText.setText(String.valueOf(currentNoteDuration));
                    durationBar.setProgress(currentNoteDuration);

                }
            }
        });
        accentMinusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAccentPitch >= (minPitch + 10)) {
                    currentAccentPitch -= 10;
                    accentText.setText(String.valueOf(currentAccentPitch));
                    accentBar.setProgress(currentAccentPitch);
                }
            }
        });
        accentMinusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAccentPitch >= (minPitch + 1)) {
                    currentAccentPitch -= 1;
                    accentText.setText(String.valueOf(currentAccentPitch));
                    accentBar.setProgress(currentAccentPitch);

                }
            }
        });
        accentPlusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAccentPitch <= (maxPitch - 10)) {
                    currentAccentPitch += 10;
                    accentText.setText(String.valueOf(currentAccentPitch));
                    accentBar.setProgress(currentAccentPitch);

                }
            }
        });
        accentPlusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAccentPitch <= (maxPitch - 1)) {
                    currentAccentPitch += 1;
                    accentText.setText(String.valueOf(currentAccentPitch));
                    accentBar.setProgress(currentAccentPitch);

                }
            }
        });
        beatMinusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentBeatPitch >= (minPitch + 10)) {
                    currentBeatPitch -= 10;
                    beatText.setText(String.valueOf(currentBeatPitch));
                    beatBar.setProgress(currentBeatPitch);

                }
            }
        });
        beatMinusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentBeatPitch >= (minPitch + 1)) {
                    currentBeatPitch -= 1;
                    beatText.setText(String.valueOf(currentBeatPitch));
                    beatBar.setProgress(currentBeatPitch);

                }
            }
        });
        beatPlusTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentBeatPitch <= (maxPitch - 10)) {
                    currentBeatPitch += 10;
                    beatText.setText(String.valueOf(currentBeatPitch));
                    beatBar.setProgress(currentBeatPitch);

                }
            }
        });
        beatPlusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentBeatPitch <= (maxPitch - 1)) {
                    currentBeatPitch += 1;
                    beatText.setText(String.valueOf(currentBeatPitch));
                    beatBar.setProgress(currentBeatPitch);

                }
            }
        });
        beatPreviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                metronome.playSample(currentBeatPitch, currentNoteDuration);
            }
        });

        accentPreviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                metronome.playSample(currentAccentPitch, currentNoteDuration);
            }
        });

        resetDefaults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentBeatPitch = Integer.parseInt(prefs.getDefaultBeatPitch());
                currentAccentPitch = Integer.parseInt(prefs.getDefaultAccentPitch());
                currentNoteDuration = Integer.parseInt(prefs.getDefaultNoteDuration());

                noteDurationText.setText(String.valueOf(currentNoteDuration));
                beatText.setText(String.valueOf(currentBeatPitch));
                accentText.setText(String.valueOf(currentAccentPitch));

                durationBar.setProgress(currentNoteDuration);
                accentBar.setProgress(currentAccentPitch);
                beatBar.setProgress(currentBeatPitch);
            }
        });

        resetNatural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentBeatPitch = Integer.parseInt(prefs.getNaturalNoteDuration());
                currentAccentPitch = Integer.parseInt(prefs.getNaturalAccentPitch());
                currentNoteDuration = Integer.parseInt(prefs.getNaturalBeatPitch());

                noteDurationText.setText(String.valueOf(currentNoteDuration));
                beatText.setText(String.valueOf(currentBeatPitch));
                accentText.setText(String.valueOf(currentAccentPitch));

                durationBar.setProgress(currentNoteDuration);
                accentBar.setProgress(currentAccentPitch);
                beatBar.setProgress(currentBeatPitch);
            }
        });

        durationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                noteDurationText.setText("" + i);
                currentNoteDuration = seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        accentBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                accentText.setText("" + i);
                currentAccentPitch = seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        beatBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                beatText.setText("" + i);
                currentBeatPitch = seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }

}