package agrigolo.chubbyclick;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class SetlistActivity extends AppCompatActivity {

    Context myContext = this;
    Activity myActivity = this;

    RecyclerView recyclerView;
    SetlistAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.setlist_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestFilePermissions();

        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new SetlistAdapter(myContext);
        recyclerView.setAdapter(mAdapter);

        final Button addSongButton = findViewById(R.id.addSongButton);
        final Button HomeButton = findViewById(R.id.home_button);
        final ImageButton ImportButton = findViewById(R.id.import_button);
        final ImageButton ExportButton = findViewById(R.id.export_button);

        addSongButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mAdapter.addSongDialog("Add new song");
            }
        });

        ImportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (checkFilePermissions()) {

                    SimpleFileDialog FileOpenDialog = new SimpleFileDialog(myActivity, "FileOpen",
                            new SimpleFileDialog.SimpleFileDialogListener() {
                                @Override
                                public void onChosenDir(String chosenDir) {
                                    // The code in this function will be executed when the dialog OK button is pushed
                                    char lastURIChar = chosenDir.charAt(chosenDir.length() - 1);
                                    if (lastURIChar == '/') {
                                        Toast.makeText(myActivity, "Please choose a file", Toast.LENGTH_LONG).show();
                                    } else {
                                        //DO SOMETHING WITH THE FILE URI !
                                        Toast.makeText(myActivity, "Imported from file: " +
                                                chosenDir, Toast.LENGTH_LONG).show();
                                        mAdapter.readFile(chosenDir);
                                        mAdapter.writeFile(mAdapter.getDefaultFilePath());
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                    FileOpenDialog.Default_File_Name = "";
                    FileOpenDialog.chooseFile_or_Dir();
                } else {
                    Toast.makeText(myActivity, "Please grant file access permission to use this feature!", Toast.LENGTH_LONG).show();
                }
            }
        });

        ExportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (checkFilePermissions()) {
                    SimpleFileDialog FileSaveDialog = new SimpleFileDialog(myActivity, "FileSave",
                            new SimpleFileDialog.SimpleFileDialogListener() {
                                @Override
                                public void onChosenDir(String chosenDir) {
                                    // The code in this function will be executed when the dialog OK button is pushed
                                    Toast.makeText(myActivity, "Exported to file: " +
                                            chosenDir, Toast.LENGTH_LONG).show();
                                    mAdapter.writeFile(chosenDir);

                                }
                            });
                    FileSaveDialog.Default_File_Name = "setlist_export.sl";
                    FileSaveDialog.chooseFile_or_Dir();
                } else {
                    Toast.makeText(myActivity, "Please grant file access permission to use this feature!", Toast.LENGTH_LONG).show();
                }
            }
        });

        HomeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mAdapter.isPlaying()) {
                    mAdapter.stopMetronome();
                }
                finish();
            }
        });

        ItemTouchHelper.Callback callback =
                new SetlistTouchHelper(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onBackPressed() {

    }

    public boolean checkFilePermissions() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestFilePermissions() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

}