package agrigolo.chubbyclick;
import java.io.*;

public class Song implements Serializable {

    private String name;
    private double bpm;
    private int beat;

    public Song(String name, double bpm, int beat) {
        this.name = name;
        this.bpm = bpm;
        this.beat = beat;
    }

    //Constructor that defaults beat to 4 if not specified
    public Song(String name, double bpm) {
        this.name = name;
        this.bpm = bpm;
        this.beat = 4;
    }

    public String getName() {
        return this.name;
    }

    public double getBpm() {
        return this.bpm;
    }

    public int getBeat() {
        return this.beat;
    }


}
