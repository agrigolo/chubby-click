package agrigolo.chubbyclick;

public class Song {

    private String name;
    private double bpm;
    private int beat;

    public Song(String name, double bpm, int beat) {
        this.name = name;
        this.bpm = bpm;
        this.beat = beat;
    }

    public String getName() {
        return this.name;
    }

    public double getBpm() {
        return this.bpm;
    }

    public int getBeat() {
        return this.beat;
    }

}
