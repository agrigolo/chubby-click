package agrigolo.chubbyclick;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import static java.lang.Double.valueOf;

public class MainActivity extends AppCompatActivity {

    Context myContext = this;

    double current_bpm;
    int current_beat;

    BackGroundTask chubbyClick;
    TapTempo taptempo = new TapTempo();

    private class BackGroundTask extends AsyncTask<Void, Integer, String> {

        Metronome metronome = new Metronome(current_bpm, current_beat);

        @Override
        protected String doInBackground(Void... arg0) {
            metronome.play();
            return null;
        }

        public void stop() {
            metronome.stop();
        }

        public boolean isPlaying() {
            return metronome.isPlaying();
        }

        public void bpm(double new_bpm) {
            metronome.bpm(new_bpm);
        }

        public void beat(int new_beat) {
            metronome.bpm(new_beat);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        current_beat = Integer.parseInt(Preferences.getBeat(myContext, "beat"));
        current_bpm = Double.parseDouble(Preferences.getBpm(myContext, "bpm"));

        chubbyClick = new BackGroundTask();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        final SeekBar seekbar = findViewById(R.id.tempo_seek);
        final TextView bpmText = findViewById(R.id.bpm_text);
        final EditText beatText = findViewById(R.id.input_beats);
        beatText.setText(String.valueOf(current_beat));

        seekbar.setProgress((int) current_bpm);
        bpmText.setText(String.valueOf(current_bpm));

        final ImageButton aboutButton = findViewById(R.id.aboutButton);

        final Button startButton = findViewById(R.id.start_stop);
        final Button tapTempoButton = findViewById(R.id.tap_tempo);
        final Button minusOneButton = findViewById(R.id.minus_1);
        final Button minusTenButton = findViewById(R.id.minus_10);
        final Button plusOneButton = findViewById(R.id.plus_1);
        final Button plusTenButton = findViewById(R.id.plus_10);
        final Button beatPlusButton = findViewById(R.id.beat_plus);
        final Button beatMinusButton = findViewById(R.id.beat_minus);
        final Button setListButton = findViewById(R.id.setlist_button);

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               aboutDialog();
            }
        });

        tapTempoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!taptempo.tap()) {
                    Snackbar.make(v, getResources().getString(R.string.taptempo_reset), 4000).show();
                }
                current_bpm = taptempo.getAvgBpm();
                change_bpm(current_bpm);
            }
        });
        minusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm >= 1) {
                    current_bpm -= 1;
                    change_bpm(current_bpm);
                }
            }
        });
        minusTenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm >= 10) {
                    current_bpm -= 10;
                    change_bpm(current_bpm);
                }
            }
        });
        plusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm >= 1) {
                    current_bpm += 1;
                    change_bpm(current_bpm);
                }
            }
        });
        plusTenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_bpm <= 390) {
                    current_bpm += 10;
                    change_bpm(current_bpm);
                }
            }
        });
        beatMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_beat >= 1) {
                    current_beat -= 1;
                    beatText.setText(String.valueOf(current_beat));
                    Preferences.setBeat(myContext, String.valueOf(current_beat));
                    if (chubbyClick.isPlaying()) {
                        restartClick();
                    } else {
                        chubbyClick = new BackGroundTask();
                    }
                }
            }
        });

        beatPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_beat < 32) {
                    current_beat += 1;
                    beatText.setText(String.valueOf(current_beat));
                    Preferences.setBeat(myContext, String.valueOf(current_beat));
                    if (chubbyClick.isPlaying()) {
                        restartClick();
                    } else {
                        chubbyClick = new BackGroundTask();
                    }
                }
            }
        });

        beatText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                current_beat = Integer.parseInt(beatText.getText().toString());
                chubbyClick.beat(current_beat);
                Preferences.setBeat(myContext, String.valueOf(current_beat));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!chubbyClick.isPlaying()) {
                    chubbyClick.execute();
                    startButton.setText(getResources().getString(R.string.text_stop));
                } else {
                    chubbyClick.stop();
                    chubbyClick = new BackGroundTask();
                    startButton.setText(getResources().getString(R.string.text_start));
                }
            }
        });


        setListButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chubbyClick.isPlaying()) {
                    chubbyClick.stop();
                    chubbyClick = new BackGroundTask();
                    startButton.setText(getResources().getString(R.string.text_start));
                }
                Intent setListIntent = new Intent(MainActivity.this, SetlistActivity.class);
                MainActivity.this.startActivity(setListIntent);
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                bpmText.setText("" + i);
                current_bpm = valueOf(seekBar.getProgress());
                chubbyClick.bpm(current_bpm);
                Preferences.setBpm(myContext, String.valueOf(current_bpm));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });

    }

    private void restartClick() {
        chubbyClick.stop();
        chubbyClick = new BackGroundTask();
        chubbyClick.execute();
    }


    private void change_bpm(double bpm) {
        SeekBar seekbar = findViewById(R.id.tempo_seek);
        TextView bpmText = findViewById(R.id.bpm_text);
        bpmText.setText("" + bpm);
        chubbyClick.bpm(bpm);
        seekbar.setProgress((int) bpm);
        Preferences.setBpm(myContext, String.valueOf(bpm));
    }

    private void aboutDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getResources().getString(R.string.about_message));
        dialog.setTitle("Chubby Click " + BuildConfig.VERSION_NAME);
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                    }
                });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

}