package agrigolo.chubbyclick;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import agrigolo.chubbyclick.practice.PracticeFragment;
import agrigolo.chubbyclick.setlist.SetlistFragment;
import agrigolo.chubbyclick.utilities.MediaButtonsService;
import agrigolo.chubbyclick.utilities.Preferences;


public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener,
        SetlistFragment.OnFragmentInteractionListener {

    Preferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = new Preferences();
        startService(new Intent(this, MediaButtonsService.class));

        screenAlwaysOn();
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel("CHUBBYCLICK", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HomeFragment fragment = new HomeFragment();
        fragmentTransaction.replace(R.id.fragment_container, fragment, null);
        fragmentTransaction.commit();

        bottomNavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        switch (item.getItemId()) {
                            case R.id.navigation_home:
                                fragmentTransaction.replace(R.id.fragment_container, new HomeFragment(), null);
                                fragmentTransaction.commit();
                                return true;

                            case R.id.navigation_practice:
                                fragmentTransaction.replace(R.id.fragment_container, new PracticeFragment(), null);
                                fragmentTransaction.commit();
                                return true;

                            case R.id.navigation_setlist:
                                fragmentTransaction.replace(R.id.fragment_container, new SetlistFragment(), null);
                                fragmentTransaction.commit();
                                return true;
                                
                        }
                        return false;
                    }
                });

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(0, 0);

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    protected void screenAlwaysOn() {
        if (prefs.getScreenAlwaysOn().equals("true")) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else if (prefs.getScreenAlwaysOn().equals("false")) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
}