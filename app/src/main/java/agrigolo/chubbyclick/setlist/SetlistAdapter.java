package agrigolo.chubbyclick.setlist;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import agrigolo.chubbyclick.R;
import agrigolo.chubbyclick.Song;
import agrigolo.chubbyclick.metronome.Metronome;

public class SetlistAdapter extends RecyclerView.Adapter<SetlistAdapter.MyViewHolder> implements agrigolo.chubbyclick.setlist.SetlistTouchHelper.ItemTouchHelperContract {

    private Context setListContext;

    private double current_bpm;
    private int current_beat;
    private String beatSubdivisions;

    private boolean same_selected = false;
    private String setlistFile = "default.sl";

    Metronome metronome;

    private ArrayList<Song> data;
    private int selected_position = RecyclerView.NO_POSITION;
    private int previous_position = RecyclerView.NO_POSITION;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView songViewTitle;
        private TextView songViewBpm;
        private TextView songViewBeat;
        private AppCompatImageButton songEditButton;
        private AppCompatImageButton songDeleteButton;

        View rowView;

        MyViewHolder(View itemView) {
            super(itemView);

            rowView = itemView;
            songViewTitle = itemView.findViewById(R.id.songNameText);
            songViewBpm = itemView.findViewById(R.id.songBpmText);
            songViewBeat = itemView.findViewById(R.id.songBeatText);
            songEditButton = itemView.findViewById(R.id.editSongButton);
            songDeleteButton = itemView.findViewById(R.id.deleteSongButton);

            songEditButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editSongDialog("Edit Song", getAdapterPosition());
                }
            });

            songDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteSong("Delete song ?", getAdapterPosition());
                }
            });
            //Tap listener on item to start/stop click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playPause(getAdapterPosition());
                }
            });

        }

    }

    /*
    The next three public methods are used by the BroadcastReceiver in SetlistFragment
    to manage media buttons on external devices (Bluetooth headsets etc.)
     */
    public int getPosition() {
        if(data.size()==0) return -9999;
        //selected_position is -1 if no position is selected
        else return selected_position;
    }

    public int getPreviousPosition() {
        int previous_position=selected_position-1;
        if(data.size()==0) return -9999;
        if(previous_position < 0) return 0;
        return previous_position;
    }

    public int getNextPosition() {
        int next_position=selected_position+1;
        if(data.size()==0) return -9999;
        if(next_position > data.size()) return data.size()-1;
        return next_position;
    }

    SetlistAdapter(Context setListContext) {
        this.setListContext = setListContext;
        metronome = new Metronome(0, 0, "");
        this.data = new ArrayList<>();
        readFile(getDefaultFilePath(), true);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.songViewTitle.setText(data.get(position).getName());
        holder.songViewBpm.setText(Integer.toString((int) data.get(position).getBpm()));
        holder.songViewBeat.setText(Integer.toString(data.get(position).getBeat()));

        if (selected_position == position && !same_selected) {
            colorItem(holder, 1);

        } else {
            colorItem(holder, 0);

        }
    }

    public void playPause(int position) {
        int pos = position;
        if (pos != RecyclerView.NO_POSITION) {

            selected_position = pos;

            notifyDataSetChanged();
            current_bpm = data.get(pos).getBpm();
            current_beat = data.get(pos).getBeat();
            beatSubdivisions = setBeatSubdivisions(current_beat);

            if (selected_position == previous_position && metronome.isPlaying()) {
                same_selected = true;
                metronome.stop();
                return;
            }

            if (metronome.isPlaying()) {
                metronome.stop();
            }
            metronome = new Metronome(current_bpm, current_beat, beatSubdivisions);
            metronome.execute();
            previous_position = pos;
            same_selected = false;
        }
    }

    //Sets the color combination of the rowView and its elements
    private void colorItem(MyViewHolder holder, int mode) {
        switch (mode) {
            //Not selected
            case 0:
                holder.rowView.setBackgroundColor(Color.WHITE);
                holder.songViewTitle.setTextColor(Color.BLACK);
                holder.songViewBpm.setTextColor(Color.BLACK);
                holder.songViewBeat.setTextColor(Color.BLACK);
                break;
            //Selected
            case 1:
                holder.rowView.setBackgroundColor(Color.parseColor("#2584FF"));
                holder.songViewTitle.setTextColor(Color.WHITE);
                holder.songViewBpm.setTextColor(Color.WHITE);
                holder.songViewBeat.setTextColor(Color.WHITE);
                break;
            //Dragged
            case 2:
                holder.rowView.setBackgroundColor(Color.GRAY);
                holder.songViewTitle.setTextColor(Color.BLACK);
                holder.songViewBpm.setTextColor(Color.BLACK);
                holder.songViewBeat.setTextColor(Color.BLACK);
                break;
        }

    }

    private String setBeatSubdivisions(int beat) {
        String beatSubdivisions = "";
        for (int i = 0; i < beat; i++) {
            if (i == 0) beatSubdivisions = "a";
            else beatSubdivisions += "b";
        }
        return beatSubdivisions;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onSwiped(int position) {
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(data, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(data, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        writeFile(getDefaultFilePath());
    }

    public void onRowSelected(MyViewHolder myViewHolder) {
        if (metronome.isPlaying()) {
            metronome.stop();
        }
        colorItem(myViewHolder, 2);
    }

    public void stopMetronome() {
        if (metronome.isPlaying()) {
            metronome.stop();
        }
    }

    public void onRowClear(MyViewHolder myViewHolder) {
        colorItem(myViewHolder, 0);

    }

    void addSongDialog(String dialog_title) {

        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.song_dialog, null, true);

        final EditText nameInput = viewInflated.findViewById(R.id.song_dialog_name);
        final EditText bpmInput = viewInflated.findViewById(R.id.song_dialog_bpm);
        final EditText beatInput = viewInflated.findViewById(R.id.song_dialog_beat);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (nameInput.length() == 0 || bpmInput.length() == 0 || beatInput.length() == 0) {
                    Toast.makeText(setListContext, "Fields cannot be empty!", Toast.LENGTH_SHORT).show();
                } else if (Double.valueOf(bpmInput.getText().toString()) < 1 || Double.valueOf(bpmInput.getText().toString()) > 400) {
                    Toast.makeText(setListContext, "BPM value must be between 1 and 400!", Toast.LENGTH_SHORT).show();
                } else if (Integer.valueOf(beatInput.getText().toString()) < 1) {
                    Toast.makeText(setListContext, "Beat value must be greater than 0!", Toast.LENGTH_SHORT).show();
                } else {
                    String song_dialog_name = nameInput.getText().toString();
                    double song_dialog_bpm = Double.valueOf(bpmInput.getText().toString());
                    int song_dialog_beat = Integer.valueOf(beatInput.getText().toString());
                    Song song = new Song(song_dialog_name, song_dialog_bpm, song_dialog_beat);

                    data.add(song);
                    notifyDataSetChanged();

                    writeFile(getDefaultFilePath());
                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void editSongDialog(String dialog_title, int position) {

        final int editPosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.song_dialog, null, true);

        final EditText nameInput = viewInflated.findViewById(R.id.song_dialog_name);
        nameInput.setText(data.get(position).getName());
        final EditText bpmInput = viewInflated.findViewById(R.id.song_dialog_bpm);
        bpmInput.setText(String.valueOf((int) data.get(position).getBpm()));
        final EditText beatInput = viewInflated.findViewById(R.id.song_dialog_beat);
        beatInput.setText(String.valueOf(data.get(position).getBeat()));
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (nameInput.length() == 0 || bpmInput.length() == 0 || beatInput.length() == 0) {
                    Toast.makeText(setListContext, "Fields cannot be empty!", Toast.LENGTH_SHORT).show();
                } else if (Double.valueOf(bpmInput.getText().toString()) < 1 || Double.valueOf(bpmInput.getText().toString()) > 400) {
                    Toast.makeText(setListContext, "BPM value must be between 1 and 400!", Toast.LENGTH_SHORT).show();
                } else if (Integer.valueOf(beatInput.getText().toString()) < 1) {
                    Toast.makeText(setListContext, "Beat value must be greater than 0!", Toast.LENGTH_SHORT).show();
                } else {

                    String song_dialog_name = nameInput.getText().toString();
                    double song_dialog_bpm = Double.valueOf(bpmInput.getText().toString());
                    int song_dialog_beat = Integer.valueOf(beatInput.getText().toString());
                    Song song = new Song(song_dialog_name, song_dialog_bpm, song_dialog_beat);

                    data.set(editPosition, song);
                    notifyDataSetChanged();

                    writeFile(getDefaultFilePath());
                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void deleteSong(String dialog_title, int position) {

        final int removePosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.delete_dialog, null, true);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                data.remove(removePosition);
                notifyDataSetChanged();

                writeFile(getDefaultFilePath());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void readFile(String path, boolean firstOpen) {
        try {
            ArrayList<Song> tempData = new ArrayList<>();

            String JSONString = getStringFromFile(path);
            JSONObject inObj = new JSONObject(JSONString);
            JSONArray array = inObj.getJSONArray("data");

            for (int i = 0; i < array.length(); i++) {
                String name, bpm, beat;
                name = array.getJSONObject(i).getString("name");
                bpm = array.getJSONObject(i).getString("bpm");
                beat = array.getJSONObject(i).getString("beat");
                Song song = new Song(name, Double.valueOf(bpm), Integer.valueOf(beat));
                tempData.add(song);
            }
            data = tempData;
            if (!path.equals(getDefaultFilePath())) {
                Toast.makeText(setListContext, "Imported from : " + path, Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (!firstOpen) {
                showError("IMPORT_ERROR");
            }
        }
    }

    String getDefaultFilePath() {
        File inFile = new File(setListContext.getFilesDir(), setlistFile);
        return inFile.getPath();
    }

    void writeFile(String path) {
        if (data.size() == 0 && !path.equals(getDefaultFilePath())) {
            showError("EMPTY_SETLIST");
            return;
        }
        JSONArray dataArray = new JSONArray();
        Iterator<Song> iterator = data.iterator();
        JSONObject outObj = new JSONObject();
        try {
            while (iterator.hasNext()) {
                final Song song = iterator.next();
                JSONObject songObj = new JSONObject();
                songObj.put("name", song.getName());
                songObj.put("bpm", Double.toString(song.getBpm()));
                songObj.put("beat", Integer.toString(song.getBeat()));
                dataArray.put(songObj);
            }
            outObj.put("data", dataArray);
        } catch (JSONException e) {
            e.printStackTrace();
            showError("EXPORT_ERROR");
        }
        String outString = outObj.toString();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            writer.write(outString);
            writer.close();
            if (!path.equals(getDefaultFilePath())) {
                Toast.makeText(setListContext, "Exported to : " + path, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            showError("EXPORT_IO_ERROR");
        }
    }

    private void showError(String errorType) {
        String errorMessage;

        switch (errorType) {
            case "IMPORT_ERROR":
                errorMessage = "Unable to import file!";
                break;
            case "EXPORT_ERROR":
                errorMessage = "Unable to export file!";
                break;
            case "EXPORT_IO_ERROR":
                errorMessage = "Unable to export file! File I/O error";
                break;
            case "EMPTY_SETLIST":
                errorMessage = "Setlist is empty, nothing to export!";
                break;
            default:
                errorMessage = "Error!";
                break;
        }

        Toast.makeText(setListContext, errorMessage, Toast.LENGTH_SHORT).show();

    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    private static String getStringFromFile(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

}