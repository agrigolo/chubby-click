package agrigolo.chubbyclick.setlist;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import agrigolo.chubbyclick.BuildConfig;
import agrigolo.chubbyclick.R;

public class SetlistFragment extends androidx.fragment.app.Fragment {

    private RecyclerView recyclerView;
    private SetlistAdapter mAdapter;
    private Activity myActivity;
    private Context myContext;
    private BroadcastReceiver br;

    private OnFragmentInteractionListener mListener;

    public SetlistFragment() {
    }

    public static SetlistFragment newInstance(String param1, String param2) {
        return new SetlistFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setlist, container, false);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) v.findViewById(R.id.toolbar);

        final FloatingActionButton addSongButton = v.findViewById(R.id.add_song_button);
        final Button HelpButton = v.findViewById(R.id.setlist_help_button);
        final Button ImportButton = v.findViewById(R.id.import_button);
        final Button ExportButton = v.findViewById(R.id.export_button);

        recyclerView = v.findViewById(R.id.recyclerView);
        final LinearLayoutManager manager = new LinearLayoutManager(myContext);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        mAdapter = new SetlistAdapter(myContext);
        recyclerView.setAdapter(mAdapter);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "agrigolo.chubbyclick.PLAY_PAUSE":
                        playPauseAction();
                        break;
                    case "agrigolo.chubbyclick.SKIP_TO_NEXT":
                        skipAction("NEXT");
                        break;
                    case "agrigolo.chubbyclick.SKIP_TO_PREVIOUS":
                        skipAction("PREVIOUS");
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("agrigolo.chubbyclick.PLAY_PAUSE");
        filter.addAction("agrigolo.chubbyclick.SKIP_TO_NEXT");
        filter.addAction("agrigolo.chubbyclick.SKIP_TO_PREVIOUS");
        getActivity().registerReceiver(br, filter);

        addSongButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mAdapter.addSongDialog("Add song");
            }
        });

        HelpButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                helpDialog();
            }
        });

        ImportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                SimpleFileDialog FileOpenDialog = new SimpleFileDialog(myActivity, "FileOpen",
                        new SimpleFileDialog.SimpleFileDialogListener() {
                            @Override
                            public void onChosenDir(String chosenDir) {
                                // The code in this function will be executed when the dialog OK button is pushed
                                char lastURIChar = chosenDir.charAt(chosenDir.length() - 1);
                                if (lastURIChar == '/') {
                                    Toast.makeText(myActivity, "Please choose a file", Toast.LENGTH_LONG).show();
                                } else {
                                    mAdapter.readFile(chosenDir, false);
                                    mAdapter.writeFile(mAdapter.getDefaultFilePath());
                                    mAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                FileOpenDialog.Default_File_Name = "";
                FileOpenDialog.chooseFile_or_Dir();
            }
        });

        ExportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SimpleFileDialog FileSaveDialog = new SimpleFileDialog(myActivity, "FileSave",
                        new SimpleFileDialog.SimpleFileDialogListener() {
                            @Override
                            public void onChosenDir(String chosenDir) {
                                mAdapter.writeFile(chosenDir);
                            }
                        });
                FileSaveDialog.Default_File_Name = "setlist_export.sl";
                FileSaveDialog.chooseFile_or_Dir();
            }
        });

        ItemTouchHelper.Callback callback =
                new SetlistTouchHelper(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myActivity = this.getActivity();
        myContext = myActivity;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mAdapter.stopMetronome();
        getActivity().unregisterReceiver(br);
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void playPauseAction() {
        int curr_position = mAdapter.getPosition();
        switch (curr_position) {
            case -9999:
                Toast.makeText(myActivity, "Setlist is empty!", Toast.LENGTH_SHORT).show();
                break;
            case -1:
                mAdapter.playPause(0);
                break;
            default:
                mAdapter.playPause(curr_position);
        }
    }

    private void skipAction(String mode) {
        int curr_position = mAdapter.getPosition();
        if(curr_position== -9999) {
            Toast.makeText(myActivity, "Setlist is empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        switch (mode) {
            case "NEXT":
                if(curr_position < (mAdapter.getItemCount()-1))
                    recyclerView.findViewHolderForAdapterPosition(curr_position+1).itemView.performClick();
                break;
            case "PREVIOUS":
                if (curr_position>0)
                    recyclerView.findViewHolderForAdapterPosition(curr_position-1).itemView.performClick();
                break;
        }
    }

    private void helpDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(myContext);
        dialog.setMessage(getResources().getString(R.string.setlist_help_message));
        dialog.setTitle("Setlist");
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                    }
                });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

}
