/*
The original code for the Metronome class was found here (UNLICENSE) :
https://github.com/MasterEx/BeatKeeper/blob/master/src/pntanasis/android/metronome/
It was modified to work asynchronously and to integrate features needed by this app
*/
package agrigolo.chubbyclick.metronome;

import android.os.AsyncTask;

import agrigolo.chubbyclick.utilities.Preferences;

public class Metronome extends AsyncTask<Void, Integer, String> {

    private int sampleRate = 8000;
    private AudioGenerator audioGenerator = new AudioGenerator(sampleRate);
    private Preferences prefs = new Preferences();
    private int accentPitch, beatPitch, tick;
    private boolean play;
    private int silence;
    private double bpm;
    private int beat;
    private String beatSubdivisions;

    private MetronomeNotification notification;
    private MetronomeListener metronomeListener;

    public Metronome(double bpm, int beat, String beatSubdivisions) {

        this.bpm = bpm;
        this.beat = beat;
        this.beatSubdivisions = beatSubdivisions;

        audioGenerator.createPlayer();

        beatPitch = Integer.valueOf(prefs.getBeatPitch());
        accentPitch = Integer.valueOf(prefs.getAccentPitch());
        tick = Integer.valueOf(prefs.getNoteDuration());
        setVolume(Double.parseDouble(prefs.getVolume()));
        notification = new MetronomeNotification();
        this.metronomeListener = null;

    }

    @Override
    protected String doInBackground(Void... arg0) {
        notification.showRunningNotification(beat, bpm);
        play();
        return null;
    }

    private void calcSilence() {
        silence = (int) (((60 / bpm) * sampleRate) - tick);
    }

    public void play() {
        this.play = true;
        int currentMeasure = 0;

        double[] accentTick =
                audioGenerator.getSineWave(this.tick, sampleRate, accentPitch);
        double[] beatTick =
                audioGenerator.getSineWave(this.tick, sampleRate, beatPitch);
        double[] muteTick =
                audioGenerator.getSineWave(this.tick, sampleRate, 0);

        double silence = 0;
        double[] sound = new double[sampleRate];
        int t = 0, s = 0, b = 0;
        do {
            //Calculate silence duration in every cycle to be able change the bpm without restarting the task
            calcSilence();
            for (int i = 0; i < sound.length && play; i++) {
                if (t < this.tick) {
                    if (beatSubdivisions.charAt(b) == 'a') {
                        sound[i] = accentTick[t];
                    } else if (beatSubdivisions.charAt(b) == 'b') {
                        sound[i] = beatTick[t];
                    } else if (beatSubdivisions.charAt(b) == 'm') {
                        sound[i] = muteTick[t];
                    }
                    t++;
                } else {
                    sound[i] = silence;
                    s++;
                    if (s >= this.silence) {
                        t = 0;
                        s = 0;
                        b++;
                        if (b > this.beat -1) {
                            b = 0;
                            currentMeasure++;
                            //Fire Measure Change event for Gaps Mode
                            if (metronomeListener != null)
                                metronomeListener.onMeasureChange(currentMeasure);
                        }
                    }
                }
            }
            audioGenerator.writeSound(sound);
        } while (this.play);
    }

    public void stop() {
        this.play = false;
        notification.hideRunningNotification();
        audioGenerator.destroyAudioTrack();
    }

    public void playSample(int freq, int duration) {

        double[] tick =
                audioGenerator.getSineWave(duration, sampleRate, freq);

        double[] sound = new double[sampleRate];
        int t = 0, s = 0, b = 0;

        for (int i = 0; i < sound.length; i++) {
            if (t < duration) {
                sound[i] = tick[t];
                t++;
            }
        }
        audioGenerator.writeSound(sound);
    }

    public interface MetronomeListener {
        public void onMeasureChange(int current_measure);
    }

    public void setMetronomeListener(MetronomeListener listener) {
        this.metronomeListener = listener;
    }

    //Getters and setters
    public boolean isPlaying() {
        return this.play;
    }

    public void setBpm(double new_bpm) {
        bpm = new_bpm;
    }

    public double getBpm() {
        return bpm;
    }

    public void setBeat(int new_beat) {
        beat = new_beat;
    }

    public void setVolume(double new_volume) {
        audioGenerator.setVolume(new_volume);
    }

    public void setBeatSubdivisions(String new_subdivisions) {
        beatSubdivisions = new_subdivisions;
    }

    public String getBeatSubdivisions() {
        return beatSubdivisions;
    }

}