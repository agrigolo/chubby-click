package agrigolo.chubbyclick.metronome;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import agrigolo.chubbyclick.Application;
import agrigolo.chubbyclick.MainActivity;
import agrigolo.chubbyclick.R;

public class MetronomeNotification {

    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;

    Context metronomeContext = Application.getContext();

    Intent notificationIntent = new Intent(metronomeContext, MainActivity.class);
    PendingIntent intent = PendingIntent.getActivity(metronomeContext, 0,
            notificationIntent, 0);


    public void showRunningNotification(int beat, double bpm) {

        String contentText = "Metronome is running - "
                + (int) bpm + " BPM, "
                + beat + " Beats";

        notificationManager = NotificationManagerCompat.from(metronomeContext);

        builder = new NotificationCompat.Builder(metronomeContext, "CHUBBYCLICK")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Chubby Click")
                .setContentIntent(intent)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        notificationManager.notify(1,
                builder.setContentText(contentText)
                        .build()
        );
    }

    public void hideRunningNotification() {
        notificationManager.cancel(1);
    }
}
