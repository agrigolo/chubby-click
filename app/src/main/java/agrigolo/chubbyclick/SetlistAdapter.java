package agrigolo.chubbyclick;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class SetlistAdapter extends RecyclerView.Adapter<SetlistAdapter.MyViewHolder> implements SetlistTouchHelper.ItemTouchHelperContract {

    Context setListContext;

    double current_bpm;
    int current_beat;
    boolean same_selected = false;
    String setlistFile = "default.sl";

    BackGroundTask chubbyClick = new BackGroundTask();

    private class BackGroundTask extends AsyncTask<Void, Integer, String> {

        Metronome metronome = new Metronome(current_bpm, current_beat);

        @Override
        protected String doInBackground(Void... arg0) {
            metronome.play();
            return null;
        }

        public void stop() {
            metronome.stop();
        }

        public boolean isPlaying() {
            return metronome.isPlaying();
        }

    }

    public void stopMetronome() {
        chubbyClick.stop();
    }

    public boolean isPlaying() {
        return chubbyClick.isPlaying();
    }

    private ArrayList<Song> data;
    private int selected_position = RecyclerView.NO_POSITION;
    private int previous_position = RecyclerView.NO_POSITION;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView songViewTitle;
        private TextView songViewBpm;
        private TextView songViewBeat;
        private AppCompatImageButton songEditButton;
        private AppCompatImageButton songDeleteButton;

        View rowView;

        public MyViewHolder(View itemView) {
            super(itemView);

            rowView = itemView;
            songViewTitle = itemView.findViewById(R.id.songNameText);
            songViewBpm = itemView.findViewById(R.id.songBpmText);
            songViewBeat = itemView.findViewById(R.id.songBeatText);
            songEditButton = itemView.findViewById(R.id.editSongButton);
            songDeleteButton = itemView.findViewById(R.id.deleteSongButton);

            songEditButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editSongDialog("Edit Song", getAdapterPosition());
                }
            });

            songDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteSong("Delete song ?", getAdapterPosition());
                }
            });
            // on item click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION) {

                        selected_position = pos;

                        notifyDataSetChanged();
                        current_bpm = data.get(pos).getBpm();
                        current_beat = data.get(pos).getBeat();

                        if (selected_position == previous_position && chubbyClick.isPlaying()) {
                            same_selected = true;
                            chubbyClick.stop();
                            return;
                        }

                        if (chubbyClick.isPlaying()) {
                            chubbyClick.stop();
                            chubbyClick = new BackGroundTask();
                            chubbyClick.execute();
                        } else {
                            chubbyClick = new BackGroundTask();
                            chubbyClick.execute();
                        }
                        previous_position = pos;
                        same_selected = false;
                    }
                }
            });

        }

    }

    public SetlistAdapter(Context setListContext) {
        this.setListContext = setListContext;
        this.data = new ArrayList<>();
        //Populates adapter data with the content of the default file
        readFile(getDefaultFilePath());

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.songViewTitle.setText(data.get(position).getName());
        holder.songViewBpm.setText(Double.toString(data.get(position).getBpm()));
        holder.songViewBeat.setText(Integer.toString(data.get(position).getBeat()));

        if (selected_position == position && same_selected == false) {
            colorItem(holder, 1);

        } else {
            colorItem(holder, 0);

        }
    }

    //Sets the color combination of the rowView and its elements
    public void colorItem(MyViewHolder holder, int mode) {
        switch (mode) {
            //Not selected
            case 0:
                holder.rowView.setBackgroundColor(Color.WHITE);
                holder.songViewTitle.setTextColor(Color.BLACK);
                holder.songViewBpm.setTextColor(Color.BLACK);
                holder.songViewBeat.setTextColor(Color.BLACK);
                break;
            //Selected
            case 1:
                holder.rowView.setBackgroundColor(Color.BLUE);
                holder.songViewTitle.setTextColor(Color.WHITE);
                holder.songViewBpm.setTextColor(Color.WHITE);
                holder.songViewBeat.setTextColor(Color.WHITE);
                break;
            //Dragged
            case 2:
                holder.rowView.setBackgroundColor(Color.GRAY);
                holder.songViewTitle.setTextColor(Color.BLACK);
                holder.songViewBpm.setTextColor(Color.BLACK);
                holder.songViewBeat.setTextColor(Color.BLACK);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onSwiped(int position) {
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(data, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(data, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        writeFile(getDefaultFilePath());
    }

    @Override
    public void onRowSelected(MyViewHolder myViewHolder) {
        if (chubbyClick.isPlaying()) {
            chubbyClick.stop();
        }
        colorItem(myViewHolder, 2);
    }

    @Override
    public void onRowClear(MyViewHolder myViewHolder) {
        colorItem(myViewHolder, 0);

    }

    void addSongDialog(String dialog_title) {

        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.song_dialog, null, true);

        final EditText nameInput = viewInflated.findViewById(R.id.song_dialog_name);
        final EditText bpmInput = viewInflated.findViewById(R.id.song_dialog_bpm);
        final EditText beatInput = viewInflated.findViewById(R.id.song_dialog_beat);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (nameInput.length() == 0 || bpmInput.length() == 0 || beatInput.length() == 0) {
                    Toast.makeText(setListContext, "Fields cannot be empty!", Toast.LENGTH_SHORT).show();
                } else {
                    String song_dialog_name = nameInput.getText().toString();
                    double song_dialog_bpm = Double.valueOf(bpmInput.getText().toString());
                    int song_dialog_beat = Integer.valueOf(beatInput.getText().toString());
                    Song song = new Song(song_dialog_name, song_dialog_bpm, song_dialog_beat);

                    data.add(song);
                    notifyDataSetChanged();

                    writeFile(getDefaultFilePath());
                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void editSongDialog(String dialog_title, int position) {

        final int editPosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.song_dialog, null, true);

        final EditText nameInput = viewInflated.findViewById(R.id.song_dialog_name);
        nameInput.setText(data.get(position).getName());
        final EditText bpmInput = viewInflated.findViewById(R.id.song_dialog_bpm);
        bpmInput.setText(String.valueOf(data.get(position).getBpm()));
        final EditText beatInput = viewInflated.findViewById(R.id.song_dialog_beat);
        beatInput.setText(String.valueOf(data.get(position).getBeat()));
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (nameInput.length() == 0 || bpmInput.length() == 0 || beatInput.length() == 0) {
                    Toast.makeText(setListContext, "Fields cannot be empty!", Toast.LENGTH_SHORT).show();
                } else {

                    String song_dialog_name = nameInput.getText().toString();
                    double song_dialog_bpm = Double.valueOf(bpmInput.getText().toString());
                    int song_dialog_beat = Integer.valueOf(beatInput.getText().toString());
                    Song song = new Song(song_dialog_name, song_dialog_bpm, song_dialog_beat);

                    data.set(editPosition, song);
                    notifyDataSetChanged();

                    writeFile(getDefaultFilePath());
                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void deleteSong(String dialog_title, int position) {

        final int removePosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(setListContext);
        builder.setTitle(dialog_title);
        View viewInflated = LayoutInflater.from(setListContext).inflate(R.layout.delete_dialog, null, true);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                data.remove(removePosition);
                notifyDataSetChanged();

                writeFile(getDefaultFilePath());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void writeFile(String path) {
        try {
            File outFile = new File(path);
            outFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(outFile.getPath());
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(setListContext, "Unable to export file!", Toast.LENGTH_SHORT).show();
        }
    }

    public void readFile(String path) {
        try {
            File inFile = new File(path);
            inFile.createNewFile();
            FileInputStream fis = new FileInputStream(inFile.getPath());
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = (ArrayList) ois.readObject();
            ois.close();
            fis.close();
        } catch (EOFException e) {
            //Do nothing for this kind of exception.
            // It normally happens when the setlist data is empty at first run
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(setListContext, "Unable to import file!", Toast.LENGTH_SHORT).show();
        }
    }

    public String getDefaultFilePath() {
        File inFile = new File(setListContext.getFilesDir(), setlistFile);
        return inFile.getPath();
    }

}