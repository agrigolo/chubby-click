package agrigolo.chubbyclick.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import agrigolo.chubbyclick.Application;

public class Preferences {

    private static String PREF_NAME = "prefs";
    //DEFAULT VALUES :
    private final static String defaultActivity = "home";
    private final static String defaultBpm = "100";
    private final static String defaultBeat = "4";
    //Default Beep sound
    private final static String defaultNoteDuration = "600";
    private final static String defaultAccentPitch = "800";
    private final static String defaultBeatPitch = "600";
    //Default Natural sound
    private final static String naturalNoteDuration = "600";
    private final static String naturalAccentPitch = "800";
    private final static String naturalBeatPitch = "60";
    private final static String defaultScreenAlwaysOn = "true";
    private final static String defaultVolume = "100.0";
    //Default Practice values
    private final static String defaultPracticeStartBPM = "100";
    private final static String defaultPracticeEndBPM = "120";
    private final static String defaultPracticeMeasures = "5";
    private final static String defaultPracticeChangeBPM = "5";
    private final static String defaultPracticeIncreaseDecrease = "increase";
    private final static String defaultPracticeStopWhenComplete = "false";
    private final static String defaultPracticePlayBars = "4";
    private final static String defaultPracticeMuteBars = "4";

    private static Context prefsContext = Application.getContext();

    private static SharedPreferences getPrefs() {
        return prefsContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getBpm() {
        return getPrefs().getString("bpm", defaultBpm);
    }

    public static void setBpm(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("bpm", input);
        editor.apply();
    }

    public static String getBeat() {
        return getPrefs().getString("beat", defaultBeat);
    }

    public void setBeat(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("beat", input);
        editor.apply();
    }

    public String getNoteDuration() {
        return getPrefs().getString("noteDuration", defaultNoteDuration);
    }

    public void setNoteDuration(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("noteDuration", input);
        editor.apply();
    }

    public String getAccentPitch() {
        return getPrefs().getString("highTickFreq", defaultAccentPitch);
    }

    public void setAccentPitch(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("highTickFreq", input);
        editor.apply();
    }

    public String getBeatPitch() {
        return getPrefs().getString("lowTickFreq", defaultBeatPitch);
    }

    public void getBeatPitch(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("lowTickFreq", input);
        editor.apply();
    }

    public String getActivity() {
        return getPrefs().getString("activity", defaultActivity);
    }

    public void setActivity(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("activity", input);
        editor.apply();
    }

    public String getScreenAlwaysOn() {
        return getPrefs().getString("screenAlwaysOn", defaultScreenAlwaysOn);
    }

    public void setScreenAlwaysOn(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("screenAlwaysOn", input);
        editor.apply();
    }

    public String getVolume() {
        return getPrefs().getString("volume", defaultVolume);
    }

    public void setVolume(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("volume", input);
        editor.apply();
    }

    public String getDefaultNoteDuration() {
        return defaultNoteDuration;
    }

    public String getDefaultAccentPitch() {
        return defaultAccentPitch;
    }

    public String getDefaultBeatPitch() {
        return defaultBeatPitch;
    }

    public String getNaturalNoteDuration() {
        return naturalNoteDuration;
    }

    public String getNaturalAccentPitch() {
        return naturalAccentPitch;
    }

    public String getNaturalBeatPitch() {
        return naturalBeatPitch;
    }

    public static String getPracticeStartBPM() {
        return getPrefs().getString("practiceStartBPM", defaultPracticeStartBPM);
    }

    public static String getPracticeEndBPM() {
        return getPrefs().getString("practiceEndBPM", defaultPracticeEndBPM);
    }

    public static String getPracticeMeasures() {
        return getPrefs().getString("practiceMeasures", defaultPracticeMeasures);
    }

    public static String getPracticeChangeBPM() {
        return getPrefs().getString("practiceChangeBPM", defaultPracticeChangeBPM);
    }

    public static String getPracticeIncreaseDecrease() {
        return getPrefs().getString("practiceIncreaseDecrease", defaultPracticeIncreaseDecrease);
    }

    public static void setPracticeStartBPM(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceStartBPM", input);
        editor.apply();
    }

    public static void setPracticeEndBPM(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceEndBPM", input);
        editor.apply();
    }

    public static void setPracticeMeasures(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceMeasures", input);
        editor.apply();
    }

    public static void setPracticeChangeBPM(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceChangeBPM", input);
        editor.apply();
    }

    public static void setPracticeIncreaseDecrease(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceIncreaseDecrease", input);
        editor.apply();
    }

    public static String getBeatSubdivisions() {
        return getPrefs().getString("beatSubdivisions", "");
    }

    public static void setBeatSubdivisions(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("beatSubdivisions", input);
        editor.apply();
    }

    public static String getPracticePlayBars() {
        return getPrefs().getString("practicePlayBars", defaultPracticePlayBars);
    }

    public static void setPracticePlayBars(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practicePlayBars", input);
        editor.apply();
    }

    public static String getPracticeMuteBars() {
        return getPrefs().getString("practiceMuteBars", defaultPracticeMuteBars);
    }

    public static void setPracticeMuteBars(String input) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString("practiceMuteBars", input);
        editor.apply();
    }

}