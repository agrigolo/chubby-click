package agrigolo.chubbyclick.utilities;

public class TapTempo {

    private int count = 0;
    private long tapStart = 0;
    private long tapTimer = 0;
    private long tapPrevious = 0;
    private double bpmAvg = 0;
    private boolean isTapping = false;

    public void tap() {
        isTapping = true;
        tapTimer = System.currentTimeMillis();

        if (count == 0) {
            tapStart = tapTimer;
            tapPrevious = tapStart;
            count = 1;

        } else {
            //Reset tempo after a 4 seconds timeout
            if ((tapTimer - tapPrevious) >= 4000) {
                count = 0;
                tapStart = 0;
                tapTimer = 0;
                tapPrevious = 0;
                bpmAvg = 0;
                return;
            }
            bpmAvg = 60000 * count / (tapTimer - tapStart);

            if (bpmAvg > 400) {
                bpmAvg = 400;
            }

            bpmAvg = Math.round(bpmAvg);
            tapPrevious = tapTimer;
            count++;
        }

    }

    public void reset() {
        count = 0;
        tapStart = 0;
        tapTimer = 0;
        tapPrevious = 0;
        bpmAvg = 0;
        isTapping = false;
    }

    public double getAvgBpm() {
        return this.bpmAvg;
    }

    public boolean isTapping() {
        return this.isTapping;
    }

}
