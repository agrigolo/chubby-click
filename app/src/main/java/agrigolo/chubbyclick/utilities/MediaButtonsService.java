package agrigolo.chubbyclick.utilities;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.media.session.MediaButtonReceiver;

public class MediaButtonsService extends Service {

    private final IBinder mBinder = new LocalBinder();

    /* Binder class the will be used by fragments */
    public class LocalBinder extends Binder {
        MediaButtonsService getService() {
            return MediaButtonsService.this;
        }
    }

    private void sendMediaButtonBroadCast(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final MediaSessionCompat.Callback callback = new MediaSessionCompat.Callback() {
        @Override
        public void onPlay() {
            sendMediaButtonBroadCast("agrigolo.chubbyclick.PLAY_PAUSE");
            super.onPlay();
        }

        public void onPause() {
            sendMediaButtonBroadCast("agrigolo.chubbyclick.PLAY_PAUSE");
            super.onPause();
        }

        public void onSkipToNext() {
            sendMediaButtonBroadCast("agrigolo.chubbyclick.SKIP_TO_NEXT");
            super.onSkipToNext();
        }

        public void onSkipToPrevious() {
            sendMediaButtonBroadCast("agrigolo.chubbyclick.SKIP_TO_PREVIOUS");
            super.onSkipToPrevious();
        }

    };

    private MediaSessionCompat mediaSession;

    @Override
    public void onCreate() {
        mediaSession = new MediaSessionCompat(this, "MEDIA");

        mediaSession.setCallback(callback);
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        PlaybackStateCompat.Builder mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(
                                PlaybackStateCompat.ACTION_PLAY |
                                PlaybackStateCompat.ACTION_PAUSE |
                                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                                PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
                                PlaybackStateCompat.ACTION_PLAY_PAUSE);

        mediaSession.setPlaybackState(mStateBuilder.build());
        mediaSession.setActive(true);
    }

    @Override
    public void onDestroy() {
        mediaSession.release();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaButtonReceiver.handleIntent(mediaSession, intent);
        return super.onStartCommand(intent, flags, startId);
    }

}


