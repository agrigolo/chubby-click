# Chubby Click

FOSS metronome for Android created for the band musician that needs to manage a setlist of songs.
The app features two sections : a conventional metronome with tap tempo , and a section where songs can be arranged in an orderable setlist.

[Get latest version! (APK Download)](https://bitbucket.org/agrigolo/chubby-click/downloads/chubby-click-1.3.apk)

## Features :

* Sound is generated, not sampled, for better accuracy
* Songs can be saved in a setlist and arranged via drag&drop
* When in setlist mode, the metronome for each song can be started and stopped with a single tap
* Setlists can be imported and exported
* Up to 400 BPM
* Tap Tempo function
* Number of beats per measure can be changed (1-32)