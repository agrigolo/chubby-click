# Development is discontinued

I wanted to take a moment to express my gratitude for the support and collaboration we've shared on this project. However, due to personal circumstances, I am no longer able to actively contribute to it.
I encourage anyone who is interested to take over, contribute, and continue the development. 
Thank you for your understanding, and I wish you all the best!

# Chubby Click

FOSS metronome for Android created for the band musician that needs to manage a setlist of songs.
The app features three sections :

- A conventional metronome with tap tempo
- A Setlist where songs can be added and sorted
- A Practice section where metronome can be programmed to automatically increase/decrease BPM or mute bars

## Features :

* Songs can be saved in a setlist and arranged via drag&drop
* When in setlist mode, the metronome for each song can be started and stopped with a single tap
* Setlists can be imported and exported
* Metronome can be programmed to auto increase/decrease BPM in the Practice section
* Metronome can be programmed to auto mute bars to help "Internal Clock" training
* The metronome can be controlled using media buttons on external devices (e.g. Bluetooth headphones)
* Sounds can be customized (notes duration,beat and accent note pitch) and two presets are provided
* Each beat in a bar can be accented or muted
* Up to 400 BPM
* Tap Tempo function
* Number of beats per measure can be changed (1-16)