Improvements
- Home screen UI reworked to be responsive
- Changed default notes pitch to 600/800 Hz
- Notification now shows BPM and Beats
- Code cleanup and optimization

Fixed
- BPM text was sometimes displayed as float when using Tap Tempo
- Sound Settings dialog : pressing Cancel did not save modifications, but selected values were not discarded on the UI