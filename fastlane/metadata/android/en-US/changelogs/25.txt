Improvements:

- The accuracy of the Auto Increase/Decrease feature in Practice mode has been increased
- The Auto Stop feature in Practice mode has been deprecated (buggy)
